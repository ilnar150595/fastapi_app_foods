#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $DB_HOST $DB_PORT; do
      sleep 1
    done
    sleep 5
    echo "PostgreSQL started"
fi

# python manage.py flush --no-input
alembic upgrade head

#gunicorn asgi:application \
#-k uvicorn.workers.UvicornWorker \
#--bind 0.0.0.0:8000 \
#--timeout 100 \
#--workers "${GUNICORN_WORKERS_NUMBER:-1}" \
#--threads "${GUNICORN_THREADS_NUMBER:-1}" \
#src.main:app

exec "$@"